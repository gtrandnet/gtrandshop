const hamburger = document.querySelector(".hamburger")
const navLinks = document.querySelector(".nav-links")
const link = document.querySelector(".menu-ul")
const bars = document.getElementById('bars')
const i = document.getElementsByClassName('fa-bars')
const cart = document.querySelector(".cart-click")
const cartList = document.querySelector(".cart-list")

hamburger.addEventListener("click", () => {
    navLinks.classList.toggle("open")
    link.classList.toggle("show")
    if (i.length > 0) {
        bars.classList.remove('fa-bars')
        bars.classList.add('fa-close')
    } else {
        bars.classList.remove('fa-close')
        bars.classList.add('fa-bars')
    }
})
cart.addEventListener("click", () => {
    cartList.classList.toggle("open")
});