<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slider;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();

        return response()->json($sliders, 200);
    }
}
