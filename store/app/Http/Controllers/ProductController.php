<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductPositionInHomePage;

class ProductController extends Controller
{
    public function getHomePageProduct()
    {
        $position = ProductPositionInHomePage::orderBy('position', 'asc')->get();
        foreach ($position as $value) {
            $products[$value->position] = [
                'img1' => $value->product->img1,
                'img2' => $value->product->img2,
                'name' => $value->product->name,
                'price' => $value->product->price,
                'old_price' => $value->product->old_price,
                'position' => $value->position,
            ];
        }
        $products = isset($products) ? $products:[];

        return response()->json($products, 200);
    }
}
