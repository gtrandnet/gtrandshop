<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Banner;

class BannerController extends Controller
{
    public function getHomePageBanners()
    {
        $banners = Banner::orderBy('position', 'asc')->get();
        $banners = isset($banners) ? $banners:[];

        return response()->json($banners, 200);
    }
}
