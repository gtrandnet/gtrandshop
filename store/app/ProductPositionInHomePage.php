<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPositionInHomePage extends Model
{
    protected $fillable = ['product_id', 'position', 'id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
