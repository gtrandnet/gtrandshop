require('./bootstrap');

window.Vue = require('vue');


// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('home-component', require('./components/HomeComponent.vue').default);
Vue.component('header-component', require('./components/Header.vue').default);
Vue.component('home-page-products', require('./components/HomePageProducts.vue').default);
Vue.component('home-page-banners', require('./components/HomePageBanners.vue').default);
Vue.component('menu-component', require('./components/Menu.vue').default);

const app = new Vue({
    el: '#app',
});