<?php

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::group(['prefix' => 'menu'], function(){
    Route::get('/category', 'CategoryController@index')->name('get.category');
});

Route::group(['prefix' => 'home-page'], function(){
    Route::get('/slider', 'SliderController@index');
    Route::get('/products', 'ProductController@getHomePageProduct');
    Route::get('/banners', 'BannerController@getHomePageBanners');
});