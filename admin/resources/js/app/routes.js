import auth from './auth/routes'
import crm from './crm/routes'
import errors from './errors/routes'

export default [...crm, ...auth, ...errors]