import { CrmHome, Slider, Positions, Banner } from '../components'
import { Home } from '../components'
import { ProductDetailTemplate } from '../components'
import { DetailTemplate } from '../components'
import { Category } from '../components'
import { SubCategory } from '../components'
import { Product } from '../components'
import { GeneralOptions, ProductDetail, ProductImages, ProductOptions, Publish, Products } from '../components'

export default [{
        path: '/',
        components: { main: CrmHome },
        name: 'crm_home',
        redirect: { name: 'home' },
        children: [{
                path: '/home',
                components: { crm: Home },
                name: 'home',
                meta: {
                    needsAuth: true
                }
            },
            {
                path: '/categories',
                components: { main: Category },
                name: 'categories'
            },
            {
                path: '/sub-categories',
                components: { main: SubCategory },
                name: 'sub_categories'
            },
            {
                path: '/all-products',
                components: { main: Products },
                name: 'all_products'
            },
            {
                path: '/add-product',
                components: { main: Product },
                name: 'add_product',
                redirect: { name: 'general_option' },
                children: [{
                        path: '/product-title',
                        components: { product: GeneralOptions },
                        name: 'general_option'
                    },
                    {
                        path: '/product-title/update/:id',
                        components: { product: GeneralOptions },
                        name: 'general_option_update'
                    },
                    {
                        path: '/product-detail',
                        components: { product: ProductDetail },
                        name: 'product_detail'
                    },
                    {
                        path: '/product-detail/update/:id',
                        components: { product: ProductDetail },
                        name: 'product_detail_update'
                    },
                    {
                        path: '/product-images',
                        components: { product: ProductImages },
                        name: 'product_images'
                    },
                    {
                        path: '/product-images/update/:id',
                        components: { product: ProductImages },
                        name: 'product_images_update'
                    },
                    {
                        path: '/product-options',
                        components: { product: ProductOptions },
                        name: 'product_options'
                    },
                    {
                        path: '/product-options/update/:id',
                        components: { product: ProductOptions },
                        name: 'product_options_update'
                    },
                    {
                        path: '/publish-product',
                        components: { product: Publish },
                        name: 'publish_product'
                    }
                ]
            },
            {
                path: '/product-detail-templete',
                components: { main: ProductDetailTemplate },
                name: 'product_detail_templete'
            },
            {
                path: '/detail-templete',
                components: { main: DetailTemplate },
                name: 'detail_templete'
            },
            {
                path: '/slider',
                components: { main: Slider },
                name: 'slider'
            },
            {
                path: '/positions',
                components: { main: Positions },
                name: 'positions'
            },
            {
                path: '/banners',
                components: { main: Banner },
                name: 'banners'
            }
        ],
        meta: {
            needsAuth: true
        }
    },

]