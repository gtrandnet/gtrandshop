import Vue from 'vue'

export const CrmHome = Vue.component('crm-home-page', require('./CrmHome.vue').default)
export const Home = Vue.component('home', require('./main/Home.vue').default)
export const CategoryModel = Vue.component('category-model', require('./main/CategoryModel.vue').default)
export const StatisticNewOrders = Vue.component('statistic-new-orders', require('./main/StatisticNewOrders.vue').default)
export const Category = Vue.component('category', require('./main/Category.vue').default)
export const SubCategory = Vue.component('sub-category', require('./main/SubCategory.vue').default)
    /*product*/
export const Product = Vue.component('product', require('./product/Product.vue').default)
export const ProductDetailTemplate = Vue.component('product-detail', require('./product/ProductDetailTemplate.vue').default)
export const DetailTemplate = Vue.component('detail-template', require('./product/DetailTemplate.vue').default)
export const GeneralOptions = Vue.component('general-options', require('./product/GeneralOptions.vue').default)
export const ProductDetail = Vue.component('product-detail', require('./product/ProductDetail.vue').default)
export const ProductImages = Vue.component('product-images', require('./product/ProductImages.vue').default)
export const ProductOptions = Vue.component('product-options', require('./product/ProductOptions.vue').default)
export const Publish = Vue.component('publish-product', require('./product/Publish.vue').default)
export const Products = Vue.component('all-products', require('./product/Products.vue').default)
    /**home page */
export const Slider = Vue.component('slider', require('./home_page/Slider.vue').default)
export const Positions = Vue.component('positions', require('./home_page/Positions.vue').default)
export const Banner = Vue.component('banner', require('./home_page/Banner.vue').default)