export const rooms = ({ dispatch }, { context }) => {
    return axios.get('/api/rooms').then((response) => {
        context.rooms = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const categories = ({ dispatch }, { context }) => {
    return axios.get('/api/categories').then((response) => {
        context.items = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const create_category = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/create/', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const update_category = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/update-category/${payload.id}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const destroy_category = ({ dispatch }, { payload, context }) => {
        return axios.delete(`/api/category/destroy/${payload.id}`).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
        })
    }
    /*sub-categories*/
export const sub_categories = ({ dispatch }, { context, payload }) => {
    return axios.get(`/api/sub-category/${payload.cat}`).then((response) => {
        context.sub_categories = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const create_sub_category = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/sub-category/create`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const update_sub_category = ({ dispatch }, { payload, context }) => {
    return axios.patch(`/api/sub-category/update/${payload.id}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const destroy_sub_category = ({ dispatch }, { payload, context }) => {
        return axios.delete(`/api/sub-category/destroy/${payload.id}`).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
        })
    }
    /**template details */
export const templates = ({ dispatch }, { context }) => {
    return axios.get('/api/template/').then((response) => {
        context.templates = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const create_template = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/template/create/', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const update_template = ({ dispatch }, { payload, context }) => {
    return axios.patch(`/api/template/update/${payload.id}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const destroy_template = ({ dispatch }, { payload, context }) => {
        return axios.delete(`/api/template/destroy/${payload.id}`).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
        })
    }
    /**product template details */
export const detailes = ({ dispatch }, { context, payload }) => {
    return axios.get(`/api/detail/${payload.template}`).then((response) => {
        context.detailes = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
    })
}
export const create_detail = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/detail/create/', payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const update_detail = ({ dispatch }, { payload, context }) => {
    return axios.patch(`/api/detail/update/${payload.id}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const destroy_detail = ({ dispatch }, { payload, context }) => {
        return axios.delete(`/api/detail/destroy/${payload.id}`).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
        })
    }
    /**product general option */
export const product_general_option_create = ({ dispatch }, { payload, context }) => {
    return axios.post('/api/product/general/create/', payload, {
        headers: {
            'Content-Type': 'multipart/form-data'
        }
    }).then((response) => {
        context.message = response.data.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}

export const product_general_option_update = ({ dispatch }, { payload, context }) => {
        return axios.post('/api/product/general/update/', payload, {
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        }).then((response) => {
            context.message = response.data.data
        }).catch((error) => {
            context.errors = error.response.data.errors
            context.e = true
        })
    }
    /**search product */

export const search_product = ({ dispatch }, { payload, context }) => {
        return axios.post('/api/product/search', payload).then((response) => {
            context.products = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
            context.e = true
        })
    }
    /**product detailes */
export const product_detail_create = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/product/detail/create/${payload.product}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const product_detail_update = ({ dispatch }, { payload, context }) => {
        return axios.patch(`/api/product/detail/update/${payload.product}`, payload).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
            context.e = true
        })
    }
    /**product images */
export const product_image_create = ({ dispatch }, { payload, context, item }) => {
    return axios.post(`/api/product/image/create`, payload, {
        onUploadProgress: (itemUpload) => {
            context.fileProgress = Math.round((itemUpload.loaded / itemUpload.total) * 100)
            context.fileCurrent = item.name + ' ' + context.fileProgress
        }
    }).then((response) => {
        context.fileProgress = 0
        context.fileCurrent = ''
        context.filesFinish.push(item)
        context.filesOrder.splice(item, 1)
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const product_image_destroy = ({ dispatch }, { payload, context }) => {
    return axios.delete(`/api/product/image/destroy/${payload.img}`).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const product_image_update = ({ dispatch }, { payload, context }) => {
        return axios.post(`/api/product/image/update`, payload).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
            context.e = true
        })
    }
    /**product option */
export const product_option_create = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/product/option/create/${payload.product}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const product_option_update = ({ dispatch }, { payload, context }) => {
        return axios.post(`/api/product/option/update`, payload).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
            context.e = true
        })
    }
    /**publish_product */
export const publish_product = ({ dispatch }, { payload, context }) => {
    return axios.patch(`/api/product/publish/${payload.product}`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}
export const unpublish_product = ({ dispatch }, { payload, context }) => {
        return axios.patch(`/api/product/unpublish/${payload.product}`, payload).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.errors = error.response.data.errors
            context.e = true
        })
    }
    /**get product */
export const get_product = ({ dispatch }, { payload, context }) => {
        return axios.get(`/api/product/show/${payload.product}`).then((response) => {
            context.product_arr = response.data
        }).catch((error) => {
            context.e = true
            context.errors = error.response.data.errors
        })
    }
    /**all_products */
export const all_products = ({ dispatch }, { context, payload }) => {
    return axios.post(`api/product/all?page=${payload.page}`, payload).then((response) => {
        context.products = response.data.data
        context.pagination = response.data.pagination
    }).catch((error) => {
        context.errors = error.response.data.errors
        context.e = true
    })
}

export const destroy_product = ({ dispatch }, { payload, context }) => {
        return axios.delete(`/api/product/destroy/${payload.product}`).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.e = true
            context.errors = error.response.data.errors
        })
    }
    /**home page  */

/**slider */

export const slider_img_create = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/home-page/slider/create`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}

export const get_slider_pages = ({ dispatch }, { context }) => {
    return axios.get(`/api/home-page/slider`).then((response) => {
        context.sliders = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}

export const slider_img_update = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/home-page/slider/update-img`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}

export const slider_text_save = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/home-page/slider/save-text`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}
export const destroy_slider = ({ dispatch }, { payload, context }) => {
        return axios.delete(`/api/home-page/slider/destroy/${payload.id}`).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.e = true
            context.errors = error.response.data.errors
        })
    }
    /**product position in home-page */

export const get_product_position = ({ dispatch }, { context }) => {
    return axios.get(`/api/home-page/position`).then((response) => {
        context.positions = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}

export const create_product_position = ({ dispatch }, { payload, context }) => {
        return axios.post(`/api/home-page/position/create`, payload).then((response) => {
            context.message = response.data
        }).catch((error) => {
            context.e = true
            context.errors = error.response.data.errors
        })
    }
    /**banners in home page */
export const get_banners = ({ dispatch }, { context }) => {
    return axios.get(`/api/home-page/banner`).then((response) => {
        context.banners = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}

export const banner_text_save = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/home-page/banner/text/create`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}

export const banner_img_update = ({ dispatch }, { payload, context }) => {
    return axios.post(`/api/home-page/banner/img/create`, payload).then((response) => {
        context.message = response.data
    }).catch((error) => {
        context.e = true
        context.errors = error.response.data.errors
    })
}