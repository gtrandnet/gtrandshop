<?php

use Illuminate\Http\Request;

Route::post('/register', 'Auth\AuthController@register');
Route::post('/login', 'Auth\AuthController@login');
Route::post('/logout', 'Auth\AuthController@logout');

Route::group(['middleware' => 'jwt.auth'], function(){
    Route::get('/me', 'Auth\AuthController@user');
    Route::post('/destroy/{id}', 'Crm\Room\RoomController@destroy');
    Route::post('/create', 'Crm\CategoryController@store');
    Route::post('/update-category/{cat}', 'Crm\CategoryController@update');
    Route::get('/get-room/{id}', 'Crm\Room\RoomController@getRoom');
    Route::get('/rooms', 'Crm\Room\RoomController@index');
    Route::get('/categories', 'Crm\CategoryController@index');
    Route::delete('/category/destroy/{id}', 'Crm\CategoryController@destroy');

    /*sub-categories*/
    Route::group(['prefix'=> 'sub-category'], function(){
        Route::get('/{cat}', 'Crm\SubCategoryController@index');
        Route::post('/create', 'Crm\SubCategoryController@store');
        Route::delete('/destroy/{id}', 'Crm\SubCategoryController@destroy');
        Route::patch('/update/{sub}', 'Crm\SubCategoryController@update');
    });
    /**template details */
    Route::group(['prefix' => 'template'], function(){
        Route::get('/', 'Crm\DetailTemplateController@index');
        Route::post('/create', 'Crm\DetailTemplateController@store');
        Route::patch('/update/{template}', 'Crm\DetailTemplateController@update');
        Route::delete('/destroy/{template}', 'Crm\DetailTemplateController@destroy');
    });
    /**product template details */
    Route::group(['prefix' => 'detail'], function(){
        Route::get('/{template}', 'Crm\ProductDetailTemplateController@index');
        Route::post('/create', 'Crm\ProductDetailTemplateController@store');
        Route::patch('/update/{detail}', 'Crm\ProductDetailTemplateController@update');
        Route::delete('/destroy/{detail}', 'Crm\ProductDetailTemplateController@destroy');
    });
    /**product */
    Route::group(['prefix' => 'product'], function(){
        Route::group(['prefix' => 'general'], function(){
            Route::post('/create', 'Crm\ProductController@store');
            Route::post('/update', 'Crm\ProductController@update');
        });
        Route::post('/search', 'Crm\ProductController@search');
        Route::post('/detail/create/{product}', 'Crm\ProductDetailController@store');
        Route::patch('/detail/update/{product}', 'Crm\ProductDetailController@update');
        Route::post('/image/create', 'Crm\ProductImageController@store');
        Route::post('/image/update', 'Crm\ProductImageController@update');
        Route::delete('/image/destroy/{img}', 'Crm\ProductImageController@destroy');
        Route::post('/option/create/{product}', 'Crm\ProductOptionController@store');
        Route::post('/option/update', 'Crm\ProductOptionController@update');
        Route::patch('/publish/{product}', 'Crm\ProductController@publish');
        Route::patch('/unpublish/{product}', 'Crm\ProductController@unpublish');
        Route::post('/all', 'Crm\ProductController@showAll');
        Route::delete('/destroy/{product}', 'Crm\ProductController@destroy');
        Route::get('/show/{product}', 'Crm\ProductController@show');
    });
    /**home-page */
    Route::group(['prefix' => 'home-page'], function(){
        Route::group(['prefix' => 'slider'], function(){
            Route::post('/create', 'Crm\SliderController@store');
            Route::get('/', 'Crm\SliderController@index');
            Route::post('/update-img', 'Crm\SliderController@updateImg');
            Route::post('/save-text', 'Crm\SliderController@saveText');
            Route::delete('/destroy/{slider}', 'Crm\SliderController@destroy');
        });
        Route::get('/position', 'Crm\ProductPositionInHomePageController@index');
        Route::post('/position/create', 'Crm\ProductPositionInHomePageController@store');
        Route::get('/banner', 'Crm\BannerController@index');
        Route::post('/banner/text/create', 'Crm\BannerController@saveText');
        Route::post('/banner/img/create', 'Crm\BannerController@saveImg');
    });
});


