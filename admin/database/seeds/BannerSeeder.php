<?php

use Illuminate\Database\Seeder;
use App\Crm\Banner;

class BannerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $b1 = Banner::create([
            'first_text' => 'first_text1',
            'second_text' => 'second_text1',
            'position' => 1,
        ]);
        $b2 = Banner::create([
            'first_text' => 'first_text2',
            'second_text' => 'second_text2',
            'position' => 2,
        ]);
    }
}
