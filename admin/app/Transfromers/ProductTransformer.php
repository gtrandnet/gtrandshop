<?php

namespace App\Transformers;

use League\Fractal\TransformerAbstract;
use App\Crm\Product;

class RoomTransformer extends TransformerAbstract
{
    public function transform(Product $product)
    {
        return [
            'id' => $product->id,
            'name' => $product->name,
            'img' => $product->img1,
            'category' => $product->category->name,
            'sub_category' => $product->sub_category->name,
        ];
    }
}