<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\ProductImage;
use App\Crm\Product;
use Intervention\Image\Facades\Image;

class ProductImageController extends Controller
{
    public function store(Request $request)
    {
        $product = Product::findOrFail($request->product);
        $img = $request->image;
        $img_name = hash('sha256', now()). '.' . $img->getClientOriginalExtension();
        Image::make($img)->resize('1000', '1000')->save(public_path('img/products/').$img_name);

        $product_image = new ProductImage;
        $product_image->img = $img_name;
        $product->productImages()->save($product_image);

        return response()->json('saved', 200);
    }

    public function update(Request $request)
    {
        $img = ProductImage::findOrFail($request->id);
        if(\File::exists(public_path('img/products/'.$img->img)))
        {
            \File::delete(public_path('img/products/'.$img->img));
        }
        $image = $request->img;
        $img_name = hash('sha256', now()). '.' . $image->getClientOriginalExtension();
        Image::make($image)->resize('1000', '1000')->save(public_path('img/products/').$img_name);
        $img->img = $img_name;
        $img->save();

        return response()->json('updated', 200);
    }

    public function destroy(ProductImage $img)
    {
        if(\File::exists(public_path('img/products/'.$img->img)))
        {
            \File::delete(public_path('img/products/'.$img->img));
        }
        $img->delete();

        return response()->json('deleted', 200);
    }
}
