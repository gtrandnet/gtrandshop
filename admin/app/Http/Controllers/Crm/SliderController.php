<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\Slider;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::all();

        return response()->json($sliders, 200);
    }

    public function store(Request $request)
    {
        $validator = $this->validate(request(), [
            'img'  => 'required|mimes:png|dimensions:min_width=200,min_height=500',
        ] );
        $img = $request->img;
        $img_name = hash('sha256', now()). '.' . $img->getClientOriginalExtension();
        Image::make($img)->resize('200', '500')->save(public_path('img/sliders/').$img_name);
        $slider = new Slider;
        $slider->img = $img_name;
        $slider->save();

        return response()->json('saved', 200);
    }

    public function updateImg(Request $request)
    {
        $validator = $this->validate(request(), [
            'img'  => 'required|mimes:png|dimensions:min_width=200,min_height=500',
        ] );
        $img = $request->img;
        $img_name = hash('sha256', now()). '.' . $img->getClientOriginalExtension();
        Image::make($img)->resize('200', '500')->save(public_path('img/sliders/').$img_name);
        $slider = Slider::findOrFail($request->id);
        if(\File::exists(public_path('img/sliders/'.$slider->img)))
        {
            \File::delete(public_path('img/sliders/'.$slider->img));
        }
        $slider->img = $img_name;
        $slider->save();

        return response()->json('updated', 200);
    }

    public function saveText(Request $request)
    {
        $validator = $this->validate(request(), [
            'first_text'  => 'required|string',
            'second_text'  => 'required|string',
        ] );
        $slider = Slider::findOrFail($request->id);
        $slider->first_text = $request->first_text;
        $slider->second_text = $request->second_text;
        $slider->save();

        return response()->json('saved', 200);
    }

    public function destroy(Slider $slider)
    {
        if(\File::exists(public_path('img/sliders/'.$slider->img)))
        {
            \File::delete(public_path('img/sliders/'.$slider->img));
        }
        $slider->delete();

        return response()->json('deleted', 200);
    }
}
