<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\ProductDetailTemplate;
use App\Crm\DetailTemplate;
use App\Http\Requests\TemplateDetailRequest;

class ProductDetailTemplateController extends Controller
{
    public function index(DetailTemplate $template)
    {
        return response()->json($template->detailes, 200);
    }
    public function store(TemplateDetailRequest $request)
    {
        $template = DetailTemplate::findOrFail($request->template);
        $detail = new  ProductDetailTemplate;
        $detail->key = $request->key;
        $detail->value = $request->value;
        $template->detailes()->save($detail);

        return response()->json('saved', 200);
    }

    public function update(TemplateDetailRequest $request, ProductDetailTemplate $detail)
    {
        $detail->key = $request->key;
        $detail->value = $request->value;
        $detail->save();

        return response()->json('updated', 200);
    }

    public function destroy(ProductDetailTemplate $detail)
    {
        $detail->delete();

        return response()->json('deleted', 200);
    }
}
