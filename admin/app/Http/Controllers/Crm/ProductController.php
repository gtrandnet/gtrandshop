<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use App\Http\Requests\ProductGeneralUpdateRequest;
use App\Crm\Product;
use Intervention\Image\Facades\Image;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

class ProductController extends Controller
{
    public function showAll(Request $request)
    {
        $products = $request->published ? Product::where('published', 1)->paginate(3) : Product::paginate(3);
        foreach ($products as $value){
            $data[] = [
                'id' => $value->id,
                'name' => $value->name,
                'img' => $value->img1,
                'published' => $value->published,
                'category' => $value->category->name,
                'sub_category' => $value->sub_category->name,
                'price' => $value->price,
            ];
        }
        $row = count($products) > 0 ? $data:[];
        return response()->json([
            'data' => $row,
            'pagination' => $products
        ], 200);
    }

    public function store(ProductRequest $request)
    {
        $product = new Product;
        $img1 = $request->img1;
        $img2 = $request->img2;
        $img1_name = hash('sha256', now()). '.' . $img1->getClientOriginalExtension();
        $img2_name = hash('sha256', now()->addMinute(1)). '.' . $img2->getClientOriginalExtension();
        Image::make($img1)->resize('200', '200')->save(public_path('img/products/').$img1_name);
        Image::make($img2)->resize('200', '200')->save(public_path('img/products/').$img2_name);

        $product->name = $request->name;
        $product->categories_id = $request->categories_id;
        $product->sub_categories_id = $request->sub_categories_id;
        $product->description = $request->description;
        $product->img1 = $img1_name;
        $product->img2 = $img2_name;
        $product->price = $request->price;
        if($request->old_price > 0){
            $product->old_price = $request->old_price;
        }
        $product->save();

        return response()->json([
            'product' => $product->id,
            'data' => 'saved'
        ], 200);
    }

    public function search(Request $request)
    {
        if(!empty($request->name)){
            $prod = Product::where('name', 'like', '%'.$request->name.'%')->get();
            foreach ($prod as $value) {
                $p[] = [
                    'id' => $value->id,
                    'name' => $value->name,
                    'category' => $value->category->name,
                    'sub_category' => $value->sub_category->name,
                    'img' => $value->img1,
                    'published' => $value->published,
                ];
            }
            $products = count($prod)>0 ? $p:[];
        }else{
            $products = [];
        }

        return response()->json($products, 200);
    }

    public function publish(Product $product)
    {
        $product->published = 1;
        $product->save();

        return response()->json('published', 200);
    }

    public function unpublish(Product $product)
    {
        $product->published = 0;
        $product->save();

        return response()->json('unpublished', 200);
    }

    public function update(ProductGeneralUpdateRequest $request)
    {
        $product = Product::findOrFail($request->product);
        $img1 = $request->img1;
        $img2 = $request->img2;
        if($img1){
            $img1_name = hash('sha256', now()). '.' . $img1->getClientOriginalExtension();
            Image::make($img1)->resize('200', '200')->save(public_path('img/products/').$img1_name);
            $product->img1 = $img1_name;
        }
        if($img2){
            $img2_name = hash('sha256', now()->addMinute(1)). '.' . $img2->getClientOriginalExtension();
            Image::make($img2)->resize('200', '200')->save(public_path('img/products/').$img2_name);
            $product->img2 = $img2_name;
        }
        $product->name = $request->name;
        $product->categories_id = $request->categories_id;
        $product->sub_categories_id = $request->sub_categories_id;
        $product->description = $request->description;
        $product->price = $request->price;
        if($request->old_price > 0){
            $product->old_price = $request->old_price;
        }
        $product->save();

        return response()->json([
            'data' => 'saved'
        ], 200);
    }

    public function destroy(Product $product)
    {
        $images[] = $product->img1;
        $images[] = $product->img2;
        foreach ($product->productImages as $value) {
            $images[] = $value->img;
        }
        foreach ($images as $value) {
            if(\File::exists(public_path('img/products/'.$value)))
            {
                \File::delete(public_path('img/products/'.$value));
            }
        }
        
        $product->delete();

        return response()->json('deleted', 200);
    }

    public function show(Product $product)
    {
        $product_arr = $product;
        $product_arr['deetailes'] = $product->detailes;
        $product_arr['images'] = $product->productImages;
        $product_arr['options'] = $product->option;
        return response()->json($product_arr, 200);
    }
}
