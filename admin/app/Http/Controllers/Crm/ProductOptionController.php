<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\ProductOption;
use App\Crm\Product;


class ProductOptionController extends Controller
{
    public function store(Request $request,Product $product)
    {
        $option = new ProductOption;
        $option->onsale = $request->onsale;
        $option->discount = $request->discount;
        $option->feature = $request->feature;
        $option->new = $request->new;
        $option->bestseller = $request->bestseller;
        $option->available = $request->available;
        $product->option()->save($option);

        return response()->json('saved', 200);
    }

    public function update(Request $request)
    {
        $option = ProductOption::findOrFail($request->id);
        $option->onsale = $request->onsale;
        $option->discount = $request->discount;
        $option->feature = $request->feature;
        $option->new = $request->new;
        $option->bestseller = $request->bestseller;
        $option->available = $request->available;
        $option->save();

        return response()->json('updated', 200);
    }
}
