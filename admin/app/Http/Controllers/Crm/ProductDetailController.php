<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\ProductDetail;
use App\Crm\Product;

class ProductDetailController extends Controller
{
    public function store(Request $request,Product $product)
    {
        if(count($request->detailes) > 0){
            foreach ($request->detailes as $value) {
                $detail = new ProductDetail;
                $detail->key = $value['key'];
                $detail->value = $value['value'];
                $product->detailes()->save($detail);
            }
        }else{
            return response()->json('Сначало добавьте детали товара', 200);
        }

        return response()->json('saved', 200);
    }

    public function update(Request $request,Product $product)
    {
        $data = $product->detailes;
        foreach ($data as $value) {
            $value->delete();
        }
        if(count($request->detailes) > 0){
            foreach ($request->detailes as $value) {
                $detail = new ProductDetail;
                $detail->key = $value['key'];
                $detail->value = $value['value'];
                $product->detailes()->save($detail);
            }
        }
        
        return response()->json('updated', 200);
    }
}
