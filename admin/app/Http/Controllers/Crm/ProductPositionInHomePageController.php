<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\ProductPositionInHomePage;

class ProductPositionInHomePageController extends Controller
{
    public function index()
    {
        $positions = ProductPositionInHomePage::all();
        foreach ($positions as $value) {
            $data[] = [
                'id' => $value->id,
                'product_id' => $value->product->id,
                'product_name' => $value->product->name,
                'product_img' => $value->product->img1,
                'position' => $value->position
            ];
        }
        $data = isset($data) ? $data:[];

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $validator = $this->validate(request(), [
            'product'  => 'required',
            'position'  => 'required|integer',
        ] );

        $position  = ProductPositionInHomePage::where('position', $request->position)->first();
        if(count($position) > 0){
            $position->position = $request->position;
            $position->product_id = $request->product;
            $position->save();
        }else{
            $position = new ProductPositionInHomePage;
            $position->position = $request->position;
            $position->product_id = $request->product;
            $position->save();
        }

        return response()->json('saved', 200);
    }
}
