<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\Banner;
use Intervention\Image\Facades\Image;

class BannerController extends Controller
{
    public function index()
    {
        $banners = Banner::all();        

        return response()->json($banners, 200);
    }

    public function saveText(Request $request)
    {
        $validator = $this->validate(request(), [
            'f'  => 'required|string',
            's'  => 'required|string',
            'pos' => 'required|integer',
        ] );
        $banner  = Banner::where('position', $request->pos)->first();
        if(count($banner) > 0){
            $banner->first_text = $request->f;
            $banner->second_text = $request->s;
            $banner->save();
        }else{
            $banner = new Banner;
            $banner->first_text = $request->f;
            $banner->second_text = $request->s;
            $banner->position = $request->pos;
            $banner->save();
        }

        return response()->json('saved', 200);
    }

    public function saveImg(Request $request)
    {
        $validator = $this->validate(request(), [
            'img'  => 'required|mimes:png|dimensions:min_width=200,min_height=500',
            'pos' => 'required|integer',
        ] );
        $img = $request->img;
        $img_name = hash('sha256', now()). '.' . $img->getClientOriginalExtension();
        Image::make($img)->resize('200', '500')->save(public_path('img/banners/').$img_name);
        $banner  = Banner::where('position', $request->pos)->first();
        if(count($banner) > 0){
            if(\File::exists(public_path('img/banners/'.$banner->img)))
            {
                \File::delete(public_path('img/banners/'.$banner->img));
            }
            $banner->img = $img_name;
            $banner->save();
        }else{
            $banner = new Banner;
            $banner->img = $img_name;
            $banner->position = $request->pos;
            $banner->save();
        }

        return response()->json('changed', 200);
    }
}
