<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\SubCategory;
use App\Crm\Category;
use App\Http\Requests\SubCategoryRequest;

class SubCategoryController extends Controller
{
    public function index(Category $cat)
    {
        return response()->json($cat->subCategories, 200);
    }

    public function store(SubCategoryRequest $request)
    {
        $sub_cat = new SubCategory;
        $sub_cat->name = $request->name;
        $sub_cat->categories_id = $request->cat;
        $sub_cat->save();

        return response()->json("saved", 200);
    }

    public function update(SubCategoryRequest $request, SubCategory $sub)
    {
        $sub->name = $request->name;
        $sub->categories_id = $request->cat;
        $sub->save();

        return response()->json("updated", 200);
    }

    public function destroy($id)
    {
        $sub = SubCategory::findOrFail($id);
        $sub->delete();

        return response()->json("deleted", 200);
    }
}
