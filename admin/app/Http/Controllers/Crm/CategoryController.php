<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\Category;
use App\Http\Requests\CategoryRequest;

class CategoryController extends Controller
{
    public function index()
    {
        $cat = Category::all();

        return response()->json($cat, 200);
    }

    public function store(CategoryRequest $request)
    {
        $cat = new Category;
        $cat->name = $request->name;
        $cat->save();

        return response()->json("saved", 200);
    }

    public function update(CategoryRequest $request, Category $cat)
    {
        $cat->name = $request->name;
        $cat->save();

        return response()->json("updated", 200);
    }

    public function destroy($id)
    {
        $cat = Category::findOrFail($id);
        $cat->delete();

        return response()->json("deleted", 200);
    }
}
