<?php

namespace App\Http\Controllers\Crm;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Crm\DetailTemplate;

class DetailTemplateController extends Controller
{
    public function index()
    {
        $data = DetailTemplate::all();

        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $validator = $this->validate(request(), [
            'name'  => 'required|unique:detail_templates|string|max:300',
        ] );
        $template = new DetailTemplate;
        $template->name = $request->name;
        $template->save();

        return response()->json("Saved", 200);
    }

    public function update(Request $request, DetailTemplate $template)
    {
        $validator = $this->validate(request(), [
            'name'  => 'required|unique:detail_templates|string|max:300',
        ] );
        $template->name = $request->name;
        $template->save();

        return response()->json("updated", 200);
    }

    public function destroy(DetailTemplate $template)
    {
        $template->delete();

        return response()->json("deleted", 200);
    }
}
