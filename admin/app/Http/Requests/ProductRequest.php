<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:products|string',
            'categories_id' => 'required',
            'sub_categories_id' => 'required',
            'description' => 'required|string',
            'img1' => 'mimes:jpg,png,jpeg|required|max:500|dimensions:min_width=200,min_height=200',
            'img2' => 'mimes:jpg,png,jpeg|required|max:500|dimensions:min_width=200,min_height=200',
            'price' => 'required',
        ];
    }
}
