<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name'];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
    }

    public function subCategories()
    {
        return $this->hasMany(SubCategory::class, 'categories_id');
    }
}
