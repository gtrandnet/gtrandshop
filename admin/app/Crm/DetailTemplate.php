<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class DetailTemplate extends Model
{
    protected $fillable = ['name'];

    public function detailes()
    {
        return $this->hasMany(ProductDetailTemplate::class, 'detail_templates_id');
    }
}
