<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class ProductDetail extends Model
{
    protected $fillable = ['key', 'value', 'product_id'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
