<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    protected $fillable = ['img'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
