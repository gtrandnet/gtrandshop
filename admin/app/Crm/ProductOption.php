<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class ProductOption extends Model
{
    protected $fillable = ['product_id', 'onsale', 'discount', 'feature', 'new', 'bestseller', 'available'];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
