<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class ProductDetailTemplate extends Model
{
    protected $fillable = ['key', 'value', 'detail_templates_id'];

    public function template()
    {
        return $this->belongsTo(DetailTemplate::class);
    }
}
