<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['first_text', 'second_text', 'img'];
}
