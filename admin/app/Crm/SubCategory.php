<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = ['name'];

    public function setNameAttribute($name)
    {
        $this->attributes['name'] = $name;
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }
}
