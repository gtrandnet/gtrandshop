<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $fillable = ['first_text', 'second_text', 'img', 'position'];
}
