<?php

namespace App\Crm;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $fillable = ['name','sub_categories_id','categories_id','price','old_price',
                            'discount','onsale','feature','new','bestseller','available','img'];
    
    public function detailes()
    {
        return $this->hasMany(ProductDetail::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'categories_id');
    }

    public function sub_category()
    {
        return $this->belongsTo(SubCategory::class, 'sub_categories_id');
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function option()
    {
        return $this->hasOne(ProductOption::class);
    }

    public function positions()
    {
        return $this->hasMany(ProductPositionInHomePage::class);
    }
}
